﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Learning;

namespace Learning_Test
{
    [TestClass]
    public class LearningTest
    {
        [TestMethod]
        public void GetSubstringCount_When_Substring_Not_Found_Returns_0()
        {
            // arrange
            string sub = "да";
            string data = @"kjdgkltgjh";

            // act
            int actual = Program.GetSubstringCount(data, sub);
            Console.WriteLine("actual=" + actual);

            // assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void GetSubstringCount_When_Substring_Found_1_Returns_1()
        {
            // arrange
            string sub = "да";
            string data = @"да";

            // act
            int actual = Program.GetSubstringCount(data, sub);

            // assert
            Assert.AreEqual(1, actual);
        }

        [TestMethod]
        public void GetSubstringCount_When_Substring_Found_2_Returns_2()
        {
            // arrange
            string sub = "да";
            string data = @"ываыдавапада";

            // act
            int actual = Program.GetSubstringCount(data, sub);

            // assert
            Assert.AreEqual(2, actual);
        }

        [TestMethod]
        public void GetSubstringCount_When_Substring_Found_3_Returns_3()
        {
            // arrange
            string sub = "да";
            string data = @"даывдааыдавапдв";

            // act
            int actual = Program.GetSubstringCount(data, sub);

            // assert
            Assert.AreEqual(3, actual);
        }
    }
}
